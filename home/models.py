from __future__ import absolute_import, unicode_literals

from django.db import models
from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from modelcluster.fields import ParentalKey
from wagtail.wagtailadmin.edit_handlers import (FieldPanel, InlinePanel, MultiFieldPanel, PageChooserPanel)
from common.models import CommonPage


class HomePage(CommonPage):
    statistic = models.BooleanField(default=False, help_text="Show statistic")
    twitter_feed = models.CharField(
        max_length=123,
        help_text="Enter twitter nickname. To disable leave clear",
        blank=True,
        null=True
    )
    body = RichTextField(blank=True, help_text="Body of article")
    content_panels = CommonPage.content_panels + [
        MultiFieldPanel([
            FieldPanel('statistic'),
            FieldPanel('twitter_feed'),
            FieldPanel('body'),
        ]),
        MultiFieldPanel(
            [
                InlinePanel('related_carousel', label="Related Carousel"),
            ],
            heading="Carousel",
            classname="collapsible collapsed"
        ),
        CommonPage.testimonial_snippet,
        CommonPage.footer_snippet,
    ]


class CarouselFields(models.Model):
    url_blog = models.ForeignKey(
        'wagtailcore.Page',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )
    url_external = models.URLField("External link", null=True, blank=True)
    image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    description = RichTextField(blank=True, help_text="Small description")

    def get_url(self):
        if self.url_external:
            return self.url_external
        elif self.url_blog:
            return self.url_blog
        else:
            return ''

    panels = [
        FieldPanel('url_external'),
        PageChooserPanel('url_blog'),
        ImageChooserPanel('image'),
        FieldPanel('description', classname="full"),
    ]

    class Meta:
        abstract = True


class HomeCarousel(Orderable, CarouselFields):
    page = ParentalKey('HomePage', related_name='related_carousel')


