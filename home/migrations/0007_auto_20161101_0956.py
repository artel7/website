# -*- coding: utf-8 -*-
# Generated by Django 1.9.10 on 2016-11-01 09:56
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0006_homepage_testimonial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='homepage',
            old_name='twter_feed',
            new_name='twitter_feed',
        ),
    ]
