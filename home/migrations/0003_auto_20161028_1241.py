# -*- coding: utf-8 -*-
# Generated by Django 1.9.10 on 2016-10-28 12:41
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import modelcluster.fields


class Migration(migrations.Migration):

    dependencies = [
        ('wagtailimages', '0013_make_rendition_upload_callable'),
        ('home', '0002_auto_20161028_1146'),
    ]

    operations = [
        migrations.CreateModel(
            name='TestimonialField',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('who_gave', models.CharField(help_text='Who gave testimonial', max_length=63)),
                ('quote', models.CharField(help_text='Testimonial quote', max_length=63)),
            ],
        ),
        migrations.CreateModel(
            name='HomeTestimonial',
            fields=[
                ('testimonialfield_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='home.TestimonialField')),
                ('sort_order', models.IntegerField(blank=True, editable=False, null=True)),
                ('page', modelcluster.fields.ParentalKey(on_delete=django.db.models.deletion.CASCADE, related_name='related_testimonial', to='home.HomePage')),
            ],
            options={
                'ordering': ['sort_order'],
                'abstract': False,
            },
            bases=('home.testimonialfield', models.Model),
        ),
        migrations.AddField(
            model_name='testimonialfield',
            name='image',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='wagtailimages.Image'),
        ),
    ]
