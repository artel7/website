from __future__ import absolute_import, unicode_literals

from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
ALLOWED_HOSTS = ['127.0.0.1', 'wip.artel7.com', 'wip.artel7.com:8000', 'localhost', 'localhost:8000', ]
ALLOWED_HOSTS = ['*']

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '&92f(5$%6mdqm7j^*_+u^2ee-=m6^^!s)2$h8@60vmrfe&vj1c'


EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'website',
        'USER': 'website',
        'PASSWORD': 'website',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}

GET_IN_TOUCH_RECIPIENTS = 'rborodinov@gmail.com'

EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'test.rborodinov@gmail.com'
EMAIL_HOST_PASSWORD = 'testsite'
EMAIL_PORT = 587
EMAIL_USE_TLS = True

# Disqus api secret = localhost-z5dnvuj8et
try:
    from .local import *
except ImportError:
    pass

