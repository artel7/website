from __future__ import unicode_literals

from django.db import models
from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField, StreamField
from modelcluster.fields import ParentalKey
from wagtail.wagtailsnippets.edit_handlers import SnippetChooserPanel
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtailadmin.edit_handlers import FieldPanel, InlinePanel, MultiFieldPanel, StreamFieldPanel

from snippet.models import TestimonialSnippet, FooterSnippet
from motionless import DecoratedMap, AddressMarker
# from puput.models import EntryPage
from puput.abstracts import EntryAbstract

from wagtail.wagtailcore import blocks
from wagtail.wagtailimages.blocks import ImageChooserBlock
from wagtail.wagtaildocs.blocks import DocumentChooserBlock
from wagtail.wagtailembeds.blocks import EmbedBlock
from wagtail.wagtailsnippets.blocks import SnippetChooserBlock
from wagtail.wagtailcore import blocks
from website.settings import base


class MessageSaveModel(models.Model):
    name = models.CharField(max_length=125, blank=True)
    mail = models.CharField(max_length=125, blank=True)
    tel = models.CharField(max_length=55, blank=True)
    message = models.TextField(blank=True)


class GoogleMapBlock(blocks.StructBlock):
    map_zoom_level = blocks.CharBlock(default=14, blank=True, max_length=3)
    address = blocks.CharBlock(default='artel7', required=True, max_length=255)

    def get_context(self, value):
        context = super(GoogleMapBlock, self).get_context(value)
        dmap = DecoratedMap(zoom=int(value['map_zoom_level']), size_x=640, size_y=400)
        dmap.add_marker(AddressMarker(value['address'].encode('utf-8'), label='A'))
        url = '{}&key={}'.format(dmap.generate_url(), base.GoogleMapsKeyAPI)
        context['map'] = url
        context['address'] = value['address']
        return context
        
    class Meta:
        template = 'google_map.html'
        icon = 'cogs'
        label = 'Google Map'


class MessageFormBlock(blocks.StructBlock):
    add_form = blocks.BooleanBlock(help_text='Show form', required=False)
    
    def __str__(self):
        return "Add Message Form"

    class Meta:
        template = 'message_form.html'
        icon = 'form'
        label = 'Message Form'


class EngagementBlock(blocks.StructBlock):
    title = blocks.CharBlock(required=True)
    image = ImageChooserBlock()
    description = blocks.TextBlock()

    def get_context(self, value):
        context = super(EngagementBlock, self).get_context(value)
        context['image'] = value['image']
        context['title'] = value['title']
        context['description'] = value['description']
        return context
    
    class Meta:
        icon = 'italic'
        label = 'Engagement'
        template = 'engagement.html'


class CarouselSimpleBlock(blocks.StructBlock):
    """docstring for CarouselBlock"""
    image = blocks.ListBlock(ImageChooserBlock)

    class Meta:
        icon = 'grip'
        label = 'Simple Carousel'
        template = 'carousel_simple.html'


class CarouselExtendedBlock(blocks.StructBlock):
    """docstring for CarouselBlock"""
    one_item = blocks.ListBlock(blocks.StructBlock([
        ('image', ImageChooserBlock()),
        ('url', blocks.URLBlock()),
        ('description', blocks.RichTextBlock()),
    ]))

    class Meta:
        icon = 'grip'
        label = 'Extended Carousel'
        template = 'carousel_extended.html'


MAIN_STREAM_FIELDS_BLOCKS = [
    ('heading', blocks.CharBlock(help_text='Title in text')),
    ('snippet', SnippetChooserBlock(TestimonialSnippet)),
    ('carousel_simple', CarouselSimpleBlock(help_text='carousel on all page')),
    ('carousel_extend', CarouselExtendedBlock()),
    ('Google_map', GoogleMapBlock()),
    ('engagements', EngagementBlock()),
    ('Message_Form', MessageFormBlock()),
    ('text', blocks.RichTextBlock()),
    ('image', ImageChooserBlock()),
    ('url', blocks.URLBlock()),
    ('boolean', blocks.BooleanBlock()),
    ('date', blocks.DateBlock()),
    ('time', blocks.TimeBlock()),
    ('date_time', blocks.DateTimeBlock()),
    ('raw_html', blocks.RawHTMLBlock()),
    ('page_chooser', blocks.PageChooserBlock()),
    ('document_chooser', DocumentChooserBlock()),
    ('embed', EmbedBlock(help_text="A field for the editor to enter a URL to a media item (such as a YouTube video) to appear as embedded media on the page.")),
    # ('Contacts', Contact(ContactSnippet, icon='placeholder')),
]


class CommonPage(Page):
    """docstring for Common"""
    meta = models.TextField(blank=True, help_text="Insert Meta Description")
    title_body = models.CharField(max_length=123, help_text="Title for about", null=True)
    description = models.CharField(max_length=255, blank=True, null=True)
    testimonial = models.ForeignKey(
        'snippet.TestimonialSnippet',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    footer = models.ForeignKey(
        'snippet.FooterSnippet',
        null=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    content_panels = [
        MultiFieldPanel([
            FieldPanel('title'),
            FieldPanel('title_body'),
            FieldPanel('description'),
        ]),
    ]

    promote_panels = Page.promote_panels + [
        MultiFieldPanel(
            [
                FieldPanel('meta', classname="full"),
            ],
        ),
    ]
    footer_snippet = SnippetChooserPanel('footer')
    testimonial_snippet = MultiFieldPanel(
        [
            SnippetChooserPanel('testimonial'),
        ],
        heading="Testimonials",
        classname="collapsible collapsed"
    )

    class Meta:
        abstract = True


class AboutPage(CommonPage):
    body = RichTextField(blank=True, help_text="About")

    content_panels = CommonPage.content_panels + [
        MultiFieldPanel([
            FieldPanel('body'),
        ]),
        MultiFieldPanel(
            [
                InlinePanel('related_banner', label="Photos for banner"),
            ],
            heading="Banner",
            classname="collapsible collapsed"
        ),
        CommonPage.testimonial_snippet,
        CommonPage.footer_snippet,
    ]


class BannerFields(models.Model):
    # url_external = models.URLField(null=True, blank=True)
    image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    # description = RichTextField(blank=True,help_text="Small description")
    panels = [
        ImageChooserPanel('image'),
        # FieldPanel('description', classname="full"),
    ]

    class Meta:
        abstract = True


class AboutBanner(Orderable, BannerFields):
    page = ParentalKey('AboutPage', related_name='related_banner')


class DefaultPage(CommonPage):
    body = StreamField(MAIN_STREAM_FIELDS_BLOCKS)
    content_panels = [
        FieldPanel('title'),
        FieldPanel('title_body'),
        StreamFieldPanel('body'),
        CommonPage.testimonial_snippet,
        CommonPage.footer_snippet,
    ]


class EntryFooterAbstract(EntryAbstract):
    footer = models.ForeignKey(
        'snippet.FooterSnippet',
        null=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    content_panels = EntryAbstract.content_panels + [
        SnippetChooserPanel('footer')
    ]
    
    class Meta:
        abstract = True
