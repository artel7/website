from django.core.mail import send_mail
from django.http import HttpResponse

from website.settings import base as settings
from .models import MessageSaveModel


def send_message(request):
    if request.is_ajax():
        if request.method == 'POST':
            try:
                message = MessageSaveModel(
                    name=request.POST['postName'],
                    mail=request.POST['postEmail'],
                    tel=request.POST['postPhone'],
                    message=request.POST['postMessage']
                )
                message.save()
            except Exception:
                pass
            try:
                send_mail(
                    'from: {}\nmail: {}\ntel: {}\n  {}'.format(
                        request.POST['postName'],
                        request.POST['postEmail'],
                        request.POST['postPhone'],
                        request.POST['postMessage']
                    ),

                    settings.EMAIL_HOST_USER,
                    [settings.GET_IN_TOUCH_RECIPIENTS],
                    fail_silently=False
                )
                return HttpResponse("success")
            except Exception as e:
                return HttpResponse(e)
    else:
        return HttpResponse('Do not pass request confirmation')
