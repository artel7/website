# -*- coding: utf-8 -*-
# Generated by Django 1.9.11 on 2016-11-04 11:46
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('snippet', '0004_auto_20161103_1529'),
        ('puput', '0003_auto_20161018_0932'),
    ]

    operations = [
        migrations.AddField(
            model_name='entrypage',
            name='footer',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='snippet.FooterSnippet'),
        ),
    ]
