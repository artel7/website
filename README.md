# Deployment on server with linux [Good article DO](https://www.digitalocean.com/community/tutorials/how-to-set-up-django-with-postgres-nginx-and-gunicorn-on-ubuntu-16-04)
### Connect to server from computer:
- `ssh root@1111.2222.3333.4444` 
-  - **and enter root password**

#### Get update and install packages
- `apt-get update`
- `apt-get install python-pip python-dev`
- `apt-get install libpq-dev libxft-dev`
- `pip install --upgrade pip`

## Install postgre data base, [example DO](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-postgresql-on-ubuntu-14-04)
- `apt-get install postgresql postgresql-contrib`

#### Create data base, change website on db_name and password
- `su postgres`
- **enter root password**
- `psql`
- `postgres=# CREATE DATABASE website;`
- `postgres=# CREATE USER website WITH password 'website';`
- `postgres=# GRANT ALL privileges ON DATABASE website TO website;`
- `postgres=# \q`
- `postgres@morpheuz:/root$ su root`
- **enter root password**

### Add new user with name: website  or other
- `useradd website`

#### Change user to website
- `su website`

#### Go to home folder
- `cd /home/`

## Install git, [example DO](https://www.digitalocean.com/community/tutorials/contributing-to-open-source-getting-started-with-git).
- `sudo apt-get install git`

#### Clone project
- `git clone https://username@bitbucket.org/artel7/website.git`
- **change username to your account on bitbucket.**

#### Make user website ouner of project directory
- `sudo chown -R website website/`
- `cd website/`

## Install virtual enviroment
- `sudo pip install virtualenv`

#### Create folder venv with virtual enviroment 
- `virtualenv venv`

#### Activate enviroment (command **deactivate** to exit from enviroment)
- `source venv/bin/activate`

#### Install programs inside enviroment
- `(venv) pip install -r requerments.txt`
- `(venv) pip install gunicorn`

#### Try [gunicorn](http://docs.gunicorn.org/en/stable/)
- `(venv) python manage.py collectstatic`
- `(venv) gunicorn --bind 0.0.0.0:8000 website.wsgi:application`
- *If you have no errors , type Ctrl+C to exit*

#### Create config file
- `(venv) nano website/gunicorn.conf.py`
-  **enter config in empty file**

 > bind = '127.0.0.1:8000'
 
 > workers = 3
 
 > user = "website"

- `(venv) deactivate`

## Install process control system: [supervisor](http://supervisord.org/), [example DO](https://www.digitalocean.com/community/tutorials/how-to-install-and-manage-supervisor-on-ubuntu-and-debian-vps)
- `pip install supervisor`
- `sudo nano /etc/supervisor/conf.d/website.conf`
- **enter config in empty file**
> [program:website]

> command=/home/website/venv/bin/gunicorn  website.wsgi:application -c /home/website/website/gunicorn.conf.py

> directory=/home/website/

> user=website

> autorestart=true

> redirect_stderr=true

#### Comands for supervisorctrl: 
- `sudo supervisorctl `
- `supervisor> reread`
- `supervisor> update`
- `supervisor> status`
- `supervisor> start website`
- `supervisor> restart website`
- `supervisor> exit`

#### We need supervisor to easy restart our site after update or any changes.

## Install nginx web server, [example DO](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-14-04-lts)
- `sudo apt-get install nginx`
- `cd /etc/nginx/sites-available/`
- `sudo nano website`
- **enter config in empty file**

> server {

>  listen 80;
>  server_name 111.222.333.44; #ip or name of domen

>  access_log  /var/log/nginx/example.log;

>  location /static/ {

>    alias /home/website/static/;

>    expires 30d;

>  }

>  location / {

>    proxy_pass http://127.0.0.1:8000; 

>    proxy_set_header Host $server_name;

>    proxy_set_header X-Real-IP $remote_addr;

>    proxy_set_header X-Forwarded-For$proxy_add_x_forwarded_for;

>   }

>}

#### Create link to config file
- `cd ../sites-enabled`
- `sudo ln -s /etc/nginx/sites-available/website website`
#### Go home 
- `cd ~`
#### Add ALLOWED_HOSTS in project settings
- `nano website/settings/dev.py`
- **replase ALLOWED_HOSTS to ALLOWED_HOSTS = ['*']** 
*or ['website.com'] #your domen or ip*


#### Start nginx server
- `sudo service nginx `
- **list of comands**
- `sudo service nginx configtest`
- **check config file**
- `sudo service nginx start `
- **start server (resrart, if server runing)**


##### If we want for any changes after connect to virual server, change user and activate enviromental
- `su website`
- `cd ~`
- `source venv/bin/activate`

##### To update project: 
- `(venv) git pull clone https://username@bitbucket.org/artel7/website.git`
- **where username is name of your bitbucket account**


