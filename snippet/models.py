from __future__ import unicode_literals

from django.db import models

from wagtail.wagtailadmin.edit_handlers import FieldPanel
from wagtail.wagtailsnippets.models import register_snippet
from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailadmin.edit_handlers import FieldPanel, InlinePanel, MultiFieldPanel, StreamFieldPanel

from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtailcore.models import Orderable, Page
from modelcluster.fields import ParentalKey
from modelcluster.models import ClusterableModel


@register_snippet
class FooterSnippet(models.Model):
    twitter = models.CharField(max_length=123, blank=True, null=True)
    facebook = models.CharField(max_length=123, blank=True, null=True)
    phone = models.CharField(max_length=23, blank=True, null=True)
    email = models.EmailField(max_length=123, blank=True, null=True)
    address = models.CharField(max_length=254, blank=True, null=True)

    def __str__(self):
        return "Footer - " + str(self.id)


@register_snippet
class TestimonialSnippet(ClusterableModel):
    """docstring for TestimonialSnippet"""
    block = models.CharField(max_length=63, help_text="Testimonial quote")
    panels = [
        FieldPanel('block'),
        InlinePanel('testimonial_item', label="One testimonial"),
    ]

    def __unicode__(self):  # __unicode__ on Python 2
        return self.block


class TestimonialItem(models.Model):
    """docstring for Testimonial"""
    quote = models.CharField(max_length=63, help_text="Testimonial quote")
    who_gave = RichTextField(help_text="Who gave testimonial, link to organisation on next line")
    image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    panels = [
        FieldPanel('quote'),
        FieldPanel('who_gave'),
        ImageChooserPanel('image'),
    ]

    class Meta:
        abstract = True


class TestimonialSnippetItem(Orderable, TestimonialItem):
    page = ParentalKey('TestimonialSnippet', related_name='testimonial_item')
