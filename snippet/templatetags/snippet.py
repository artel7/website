from django import template

register = template.Library()


@register.inclusion_tag('testimonial.html', takes_context=True)
def testimonial(context):
    return {
        'testimonial': context['page'].testimonial,
        'request': context['request'],
    }



@register.inclusion_tag('footer.html', takes_context=True)
def footer(context):
    return {
        'footer': context['page'].footer,
    }
