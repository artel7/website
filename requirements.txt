psycopg2==2.6.2
Django >=1.9,<=1.10
puput==0.7
wagtail >=1.5,<=1.6
tapioca-disqus==0.1.2
motionless==1.3
GPolyEncode==0.1.1
